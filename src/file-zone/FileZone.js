import React, {Component} from 'react';
import './FileZone.css';
import {processWord} from "../actions";
import {connect} from "react-redux";

class FileZone extends Component {

  selectedWord = (word, id) => () => {
    this.props.processWord(word, id);
  };

  render() {
    const { text } = this.props;

    return (
      <div id='file-zone'>
        <div id='file'>
          {
            (!text) ? 'Error' :
            text.map((item) => {
              const { word, id, styles } = item;
              return (
                <span key={id} className={styles}>
                  <span onDoubleClick={this.selectedWord(word, id)}>
                    {word} </span>
                </span>
              )
            })
          }
        </div>
      </div>
    );
  }
}

const mapStateToProps = ({ text }) => {
  return {
    text
  }
};

const mapDispatchToProps = {
  processWord
};

export default connect(mapStateToProps, mapDispatchToProps)(FileZone);
