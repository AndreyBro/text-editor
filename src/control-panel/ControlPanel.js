import React, {Component} from 'react';
import './ControlPanel.css';
import {addStyles} from "../actions";
import {connect} from "react-redux";

class ControlPanel extends Component {

  handleClass = (styleName) => () => {
    const { selectedWord, addStyles } = this.props;
    addStyles(selectedWord, styleName);
  };

  render() {
    return (
      <div id='control-panel'>
        <div id='format-actions'>
          <button className='format-action' type='button' onClick={this.handleClass('bold')}>
            <b>B</b>
          </button>
          <button className='format-action' type='button' onClick={this.handleClass('italic')}>
            <i>I</i>
          </button>
          <button className='format-action' type='button' onClick={this.handleClass('underline')}>
            <u>U</u>
          </button>
        </div>
      </div>
    );
  }
}

const mapStateToProps = ({ selectedWord }) => {
  return {
    selectedWord
  }
};

const mapDispatchToProps = {
  addStyles
};

export default connect(mapStateToProps, mapDispatchToProps)(ControlPanel)
