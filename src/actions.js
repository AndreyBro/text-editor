import getMockText from "./text.service";

const FETCH_TEXT = 'FETCH_TEXT';
const FETCH_TEXT_SUCCESS = 'FETCH_TEXT_SUCCESS';
const PROCESSED_WORD = 'PROCESS_WORD';
const ADD_STYLES = 'ADD_STYLES';

const fetchText = () => (dispatch) => {
  dispatch({
    type: FETCH_TEXT
  });

  getMockText()
    .then((res) => {
      dispatch({
        type: FETCH_TEXT_SUCCESS,
        payload: res
      });
    })
};

const processWord = (word, id) => (dispatch) => {
  dispatch({
    type: PROCESSED_WORD,
    payload: {
      id,
      word,
      styles: []
    }
  });
};

const addStyles = (item, styleName) => (dispatch) => {
  dispatch({
    type: ADD_STYLES,
    payload: {
      item,
      styleName
    }
  });
};

export {
  FETCH_TEXT,
  FETCH_TEXT_SUCCESS,
  PROCESSED_WORD,
  ADD_STYLES,
  fetchText,
  processWord,
  addStyles
}
