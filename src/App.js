import React, {Component} from 'react';
import './App.css';
import ControlPanel from './control-panel/ControlPanel';
import FileZone from './file-zone/FileZone';
import {fetchText} from "./actions";
import {connect} from "react-redux";

class App extends Component {
  state = {
    selectedItem: {}
  };

  componentDidMount() {
    this.props.fetchText()
  }

  render() {
    const { selectedItem } = this.state;
    const { text, selectedWord } = this.props;

    return (
      <div className='App'>
        <header>
          <span>Simple Text Editor</span>
        </header>
        <main>
          <ControlPanel
            item={selectedItem}
            addStyles={(i) => this.addStyles(i)} />
          <FileZone
            text={text}
            selectedWord={selectedWord} />
        </main>
      </div>
    );
  }
}

const mapStateToProps = ({ text }) => {
  return {
    text
  }
};

const mapDispatchToProps = {
  fetchText
};

export default connect(mapStateToProps, mapDispatchToProps)(App)
