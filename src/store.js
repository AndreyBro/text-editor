import {createStore, applyMiddleware} from 'redux';
import {FETCH_TEXT, FETCH_TEXT_SUCCESS, PROCESSED_WORD, ADD_STYLES} from './actions';
import thunk from 'redux-thunk';

const initialState = {
  text: [],
  selectedWord: {
    styles: []
    // styles: {
    //   bold: false,
    //   italic: false,
    //   underline: false
    // }
  }
};

const reducers = (state = initialState, action) => {
  switch (action.type) {

    case FETCH_TEXT:
      return {
        ...state
      };

    case FETCH_TEXT_SUCCESS:
      return {
        ...state,
        text: action.payload
        .split(' ')
        .map((word, id) => ({
          id,
          word,
          styles: []
        }))
      };

    case PROCESSED_WORD:
      return {
        ...state,
        selectedWord: action.payload
      };

    case ADD_STYLES:
      const {id, word} = action.payload.item;
      const {styleName} = action.payload;
      // const {styles} = state.selectedWord;

      // TODO: записывает только последнее значение, а мне нужно чтоб все классы добавлялись
      // const a = state.text.find((item) => {
      //   return item.id === state.selectedWord.id
      // });

      // const filterStyles = (name) => {
      //   if(styles.length > 0) {
      //     const a = [...new Set(styles)];
      //     return a;
      //   }
      //   return styles.push(name)
      // };

      const res = state.text.map((item) => {
        if (item.id === state.selectedWord.id) {
          return {
            id,
            word,
            styles: [styleName]
          }
        }
        return item
      });

      return {
        ...state,
        text: res
      };

    default:
      return state
  }
};

const store = createStore(reducers, applyMiddleware(thunk));
export {
  reducers,
  store
};
